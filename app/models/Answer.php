<?php

class Answer extends \Eloquent {
	protected $fillable = ['answer'];

	public static $rules = [
		'answer' => 'required|between:3,1000'
	];

	public function owner()
	{
		return $this->belongsTo('User', 'user_id');
	}

	public function question()
	{
		return $this->belongsTo('Question');
	}

	public function comments()
	{
		return $this->hasMany('Comment');
	}

	public function scopeNewestFirst($q)
	{
		return $q->orderBy('created_at', 'desc');
	}

	public function scopeBestFirst($q)
	{
		return $q->orderBy('vote_count', 'desc');
	}



}