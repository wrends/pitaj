<?php

class Comment extends \Eloquent {
	protected $fillable = ['comment'];

	public static $rules = [
		// 'id'		=>	'required|exists:answers, id',
		'id'		=>	'required',
		'comment' 	=>  'required|between:3,400'
	];

	public function owner()
	{
		return $this->belongsTo('User', 'user_id');
	}

	public function answer()
	{
		return $this->belongsTo('Answer');
	}

	public function question()
	{
		return $this->answer->question;
	}
}