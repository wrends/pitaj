<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $fillable = ['username', 'email', 'password'];

	public static $rules = [
		'username'				=> 'required|unique:users|alpha_dash|min:4',
		'email'				=> 'required|email',
		'password'				=> 'required|alpha_num|between:4,8|confirmed',
		'password_confirmation' => 'required|alpha_num|between:4,8'
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');


	/**
	 * Auto hash model password
	 * @param type $password
	 * @return void
	 */
	public function setPasswordAttribute($password){
		$this->attributes['password'] = Hash::make($password);
	}

	public function questions()
	{
		return $this->hasMany('Question');
	}

	public function answers()
	{
		return $this->hasMany('Answer');
	}

	public function comments()
	{
		return $this->hasMany('Comment');
	}


    public function questionVotes()
    {
        return $this->belongsToMany('Question')->withPivot('type')->withTimestamps();
    }

    public function answerVotes()
    {
        return $this->belongsToMany('Answer')->withPivot('type')->withTimestamps();
    }


    public static function top($num = 10)
    {
    	return DB::table('users')->join('answers', 'users.id', '=', 'answers.user_id')
    		->groupBy('users.username')
    		->select('users.id', 'users.username', DB::raw('count(answers.id) as answer_count'))
    		->orderBy('answer_count', 'desc')
    		->take($num)
    		->get();
    }

}
