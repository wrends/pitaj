<?php

class Tag extends \Eloquent {

	protected $fillable = ['name'];

	public function questions()
	{
		return $this->belongsToMany('Question')->withTimestamps();
	}

	public function questionsCount()
	{
		$tag = $this->questions();
		return $tag->selectRaw($tag->getForeignKey() . ', count(*) as count')->groupBy($tag->getForeignKey());

	}

	public function scopePopular($q)
	{
		return $q->get()->sortByDesc(function($q){
			return $q->profileCount();
		});
	}

}