<?php

class Vote extends \Eloquent {
	protected $guarded = ['id'];

	public function votable()
	{
		return $this->morphTo();
	}
}