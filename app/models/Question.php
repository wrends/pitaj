<?php

class Question extends \Eloquent {
	protected $fillable = ['title', 'description'];

	public static $rules = [
		'title'			=> 'required|min:10|max:255',
		'description' 	=> 'required|max:3000',
		'solved'		=> 'in:0,1'
	];

	public function owner()
	{
		return $this->belongsTo('User', 'user_id');
	}

	public function answers()
	{
		return $this->hasMany('Answer');
	}

	public function bestAnswersFirst()
	{
		return $this->hasMany('Answer')->orderBy('vote_count', 'desc');
	}

	public function newestAnswersFirst()
	{
		return $this->hasMany('Answer')->orderBy('created_at', 'desc');
	}

	public function tags()
	{
		return $this->belongsToMany('Tag')->withTimestamps();
	}

	public function scopeNewestFirst($q, $num = 10)
	{
		return $q->with('owner')->orderBy('created_at', 'desc')->take($num);
	}

	public function scopeUnanswered($q, $num = 10)
	{
		 return $q->with('answers')->has('answers', false)->take($num);
	}

	public function scopeMostViews($q, $num = 10)
	{
		 return $q->orderBy('views', 'desc')->take($num);
	}

    public function scopePopular($q, $num = 10)
    {
    	return $q->get()->sortByDesc(function($q){
    		return $q->answers->count();
    	})->take($num);

    	// return static::with('answers')->take($num)->get()
    	// 				->sortBy(function($question) {
    	// 					return $question->answers->count();
    	// 				}, 'desc');


    	// return DB::table('questions')->join('answers', 'questions.id', '=', 'answers.question_id')
    	// 	->groupBy('questions.id')
    	// 	->select('questions.*', DB::raw('count(answers.id) as answer_count'))
    	// 	->orderBy('answer_count', 'desc')
    	// 	->take($num)
    	// 	->get();
    }

	public static function search($keyword)
	{
		return static::where('title', 'LIKE', "%{$keyword}%")
					->orWhere('description', 'LIKE', "%{$keyword}%");
	}

	public function incrementTotalVisits() {
	    // increment regardless of the current value in this model.
	    $this->where('id', $this->id)->update(['totalVisits' => DB::raw('last_insert_id(totalVisits + 1)')]);

	    //update this model incase we would like to use it.
	    $this->totalVisits = DB::getPdo()->lastInsertId();

	    //remove from dirty list to prevent any saves overwriting the newer database value.
	    $this->syncOriginalAttribute('totalVisits');

	    //return it because why not
	    return $this->totalVisits;
	}

}