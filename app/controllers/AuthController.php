<?php
use QA\FormValidation\LoginForm;
use Laracasts\Validation\FormValidationException;


class AuthController extends \BaseController {

	protected $loginForm;
	public function __construct(LoginForm $loginForm)
	{
		$this->loginForm = $loginForm;

        $this->beforeFilter('auth', ['only' => ['logout']]);
        $this->beforeFilter('guest', ['only' => ['login']]);
	}

	public function login()
	{
		$input = Input::all();

		try {
			$this->loginForm->validate($input);
		} catch (FormValidationException $e) {

			return Redirect::back()->withInput()
			                       ->withErrors($e->getErrors());
		}

		if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')]))
		{
			return Redirect::intended('/');
		}

		Flash::error('Logiranje nije uspjelo zbog neispravnih podataka');
		return Redirect::back();
	}


	public function logout()
	{
		Auth::logout();
		Flash::success('Više niste prijavljeni');
		return Redirect::to('/');
	}


}