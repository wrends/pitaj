<?php

class CommentsControllerAjax extends \BaseController {

	public function __construct()
	{

	}

	/**
	 * Display a listing of the resource.
	 * GET /commentsajax
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /commentsajax/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /commentsajax
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Auth::check())
		{
			// $validation = Validator::make(Input::only('id', 'comment'), Comment::$rules);
			// if ($validation->passes()) {
				$comment = new Comment;
				$comment->answer_id = Input::get('id');
				$comment->user_id = Auth::user()->id;
				$comment->comment = Input::get('comment');
				$comment->save();
				return Response::json(['status' => 'OK',
					'data' => View::make('layout.fragments.comment', compact('comment'))->render()
				]);
			// }
			// return Response::json(['status' => 'ERROR', 'data' => $validation->messages() ]);
		}
		return Response::json(['status' => 'ERROR']);
	}

	/**
	 * Display the specified resource.
	 * GET /commentsajax/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /commentsajax/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /commentsajax/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /commentsajax/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}