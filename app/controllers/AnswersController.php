<?php

class AnswersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /answers
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /answers/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /answers
	 *
	 * @return Response
	 */
	public function store( $id )
	{
		// dd(Input::get('answer'));

		if ($question = Question::findOrFail($id)) {
			if($question->owner->id === Auth::user()->id)
			{
				Flash::error('Odgovaranje na vlastito pitanje nije dozvoljeno');
				return Redirect::back();
			}
			if($question->answers()->where('user_id', Auth::user()->id)->first())
			{
				Flash::error('Navedeno pitanje već sadrži Vaš odgovor');
				return Redirect::back();
			}

			$v = Validator::make(Input::all(), Answer::$rules);

			if ($v->passes())
			{
				$answer = new Answer;

				$answer->answer = htmlspecialchars(Input::get('answer'));
				$answer->owner()->associate(Auth::user());
				$answer->question()->associate($question);
				$answer->save();

				Flash::success('Odgovor je poslan');
				return Redirect::back();
			}

			Flash::error('Odgovor nije poslan');
			return Redirect::back()->withInput()->withErrors($v->messages());
		}

		Flash::error('Greška');
		return Redirect::back();
	}

	/**
	 * Display the specified resource.
	 * GET /answers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /answers/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($id)
	{
		$answer = Answer::find($id);

		if($answer and ($answer->owner->id == Auth::user()->id ))
			return View::make('answers.edit')->with(compact('answer'));

		return Redirect::back();
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /answers/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($id)
	{
		$answer = Answer::find($id);

		if($answer and ($answer->owner->id == Auth::user()->id ))
		{
			$v = Validator::make(Input::all(), Answer::$rules);
			if ($v->passes())
			{
				$answer->answer = htmlspecialchars(Input::get('answer'));
				$answer->save();
				Flash::success('Izmijenili ste Vaš odgovor.');
				// return View::make('answers.edit')->with(compact('answer'));
				return Redirect::route('questions.show', [$answer->question->id]);
			}
			return Redirect::back()->withInput()->withErrors($v->messages());
		}

		return Redirect::back();
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /answers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /answers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}