<?php

class RegistrationsController extends \BaseController {

	/**
	 * Show the form for creating a new resource.
	 * GET /registrations/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('registration');
	}


	/**
	 * Show the form for creating a new resource.
	 * Post /registrations/store
	 *
	 * @return Response
	 */
	public function store()
	{
		$validation = Validator::make(Input::all(), User::$rules);

		if ( ! $validation->fails()) {
			User::create(Input::all());
			Flash::success('Registracija je uspjela!');
			return Redirect::intended('/');
		}
		return Redirect::back()->withInput(Input::all())->withErrors($validation->messages());
	}

}