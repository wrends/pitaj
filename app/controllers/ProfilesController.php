<?php

class ProfilesController extends \BaseController {

	public function index()
	{
		$questions = Auth::user()->questions()->orderBy('created_at', 'desc')->paginate(10);
		$answers   = Auth::user()->answers()->orderBy('created_at', 'desc')->paginate(10);
		$comments  = Auth::user()->comments()->orderBy('created_at', 'desc')->paginate(10);

		return View::make('profile')
			->with(['questions' => $questions,
					'answers' 	=> $answers,
					'comments' 	=> $comments]);
	}

}