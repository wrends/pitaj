<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		// $questions = Question::with('owner')->orderBy('date_created', 'asc')


		return View::make('home')
			->with(['questionsUnanswered' => Question::with('owner')->unanswered()->get()])
			->with(['questionsMostViews'  => Question::with('owner')->mostViews()->get()])
			->with(['questionsPopular'    => Question::popular()])
			->with(['stats' 	  		  => ['questionTotal' => Question::count(),
											  'userTotal'	  => User::count(),
											  'answerTotal'	  => Answer::count()]
			]);
	}



}
