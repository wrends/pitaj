<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::post('/questions/{id}',		['as' => 'questions.update', 'uses' => 'QuestionsController@postUpdate']);
Route::get('/questions/{id}/edit', 	['as' => 'questions.edit', 'uses' => 'QuestionsController@getEdit']);
Route::resource('questions', 'QuestionsController');

Route::post('/question/{id}/answer', ['as' => 'answers.store', 'uses' => 'AnswersController@store']);
Route::get('/vote/questions/{id}/up', ['as' => 'questionUp', 'uses' => 'VotingController@voteUp']);
Route::get('/vote/questions/{id}/down', ['as' => 'questionDown', 'uses' => 'VotingController@voteDown']);
Route::get('/vote/answers/{id}/up', ['as' => 'answerUp', 'uses' => 'VotingController@voteUp']);
Route::get('/vote/answers/{id}/down', ['as' => 'answerDown', 'uses' => 'VotingController@voteDown']);


Route::post('/answers/{id}',		['as' => 'answers.update', 'uses' => 'AnswersController@postUpdate']);
Route::get('/answers/{id}/edit', 	['as' => 'answers.edit', 'uses' => 'AnswersController@getEdit']);

// Registration & Authentication TODO
Route::get('/register', ['as' => 'registration', 'uses' => 'RegistrationsController@create']);
Route::post('/register', ['as' => 'registration', 'uses' => 'RegistrationsController@store']);
// Route::get('/login', ['as' => 'login', 'uses' => 'AuthenticationsController@create']);
// Route::post('/login', ['as' => 'store', 'uses' => 'AuthenticationsController@store']);
// Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthenticationsController@destroy']);

Route::post('/login', ['as' => 'store', 'uses' => 'AuthController@login']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);

Route::get('/profile', 'ProfilesController@index');

Route::get('/search', ['as' => 'search', 'uses' => 'SearchController@search']);


Route::get('tags', ['as' => 'tags', 'uses' => 'TagsController@index']);
Route::get('tag/{slug}', ['as' => 'tagsearch', 'uses' => 'QuestionsController@tag']);

Route::post('/comments', 'CommentsControllerAjax@store');


/*
|--------------------------------------------------------------------------
| View Composers
|--------------------------------------------------------------------------
|
*/
View::composer('layout.fragments.sidebar', function($view)
{
	$tags = Tag::take(15)->orderBy('count', 'desc')->get();
	$topUsers = User::top();

	$view
		->with('tags', $tags)
		->with('topUsers', $topUsers);
});