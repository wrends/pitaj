@extends('layout.default')
@section('content')


<div class="row">
	<div class="container">
<div class="col-sm-8 col-sm-offset-2">
{{ $errors->first() }}

{{ Form::model( $question, ['route' => 'questions.update'] ) }}

<!-- Heading -->
<div class="sub-heading my-heading bg-info text-info">
	<h2>Uredi pitanje</h2>
</div>
<div class="hr-line-dashed"></div>

    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
        {{ Form::label('title', 'Pitanje') }}
        {{ Form::text('title', null, ['class' => 'form-control']) }}
        {{ $errors->first('title', '<p class="help-block">:message</p>') }}
    </div>

    <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
        {{ Form::label('tags', 'Tagovi') }}
        {{ Form::text('tags', $tags, ['class' => 'form-control', 'id' => 'tokenfield']) }}
        {{ $errors->first('tags', '<p class="help-block">:message</p>') }}
    </div>

	{{ Form::textarea('description', null, ['id' => 'editor']) }}
<div class="hr-line-dashed"></div>
    <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
        {{ Form::label('notify', 'Želim biti obaviješten o odgovorima putem email-a.') }}
       {{ Form::checkbox('notify', 'value') }}

    </div>
{{Form::submit('OK', ['id' => 'submit-question', 'class' => 'btn btn-lg btn-primary pull-right'])}}
{{ Form::close() }}



	</div>
	</div>
</div>
@stop