@extends('layout.default')

@section('content')

<div class="row">
<div class="col-md-8">
@include('layout.fragments.home-search')
<a href="/questions/create" class="btn btn-success my-btn-success btn-lg pull-right"><i class="fa fa-plus-circle"></i> &nbsp; Postavi pitanje</a>
<div class="tabbable">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#pane1" data-toggle="tab"><span class=""><i class="fa fa-exclamation-circle"></i>
</span>&nbsp;Neodgovorena</a></li>
    <li><a href="#pane2" data-toggle="tab"><span><i class="fa fa-fire"></i></span>&nbsp;Popularna</a></li>
    <li><a href="#pane3" data-toggle="tab"><span><i class="fa fa-comments-o"></i>
</span>&nbsp;Sa najviše odgovora</a></li>
  </ul>
  <div class="tab-content">
    <div id="pane1" class="tab-pane active">





    @foreach($questionsUnanswered as $question)
    	@include('layout.fragments.question-listing')
    @endforeach





    </div>
    <div id="pane2" class="tab-pane">
    @foreach($questionsMostViews as $question)
    	@include('layout.fragments.question-listing')
    @endforeach
    </div>
    <div id="pane3" class="tab-pane">
    @foreach($questionsPopular as $question)
      @include('layout.fragments.question-listing')
    @endforeach
    </div>

  </div><!-- /.tab-content -->
</div><!-- /.tabbable -->


</div>


<div class="col-md-3">
  @include('layout.fragments.sidebar')
</div>

</div><!--row-->
@stop