{{ Form::open( ['route' => 'search', 'method' => 'get'] ) }}
<div class="input-group">
    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
        {{ Form::text('q', null, ['class' => 'form-control input-lg', 'placeholder' => 'Pretraživanje']) }}
        {{ $errors->first('title', '<p class="help-block">:message</p>') }}
    </div>
    <span class="input-group-btn">
    <i class="fa fa-search"></i>

    {{ HTML::decode(Form::button('<i class="fa fa-search"></i>Traži', array('class' => 'btn btn-lg btn-primary', 'type' => 'submit'))) }}
    </span>
</div>
{{ Form::close( )}}