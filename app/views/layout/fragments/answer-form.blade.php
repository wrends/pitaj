@if(Auth::check())
<div class="col-sm-8 col-sm-offset-2" style=" padding-bottom: 30px;">

{{ Form::open( ['route' => ['answers.store', $question->id]] ) }}



<!-- Heading -->
<div class="sub-heading my-heading bg-info text-info">
<h2>Dodaj novi odgovor</h2>

</div>
<div class="hr-line-dashed"></div>
{{ $errors->first() }}
{{ Form::textarea('answer', null, ['id' => 'editor']) }}
{{Form::submit('Odgovori', ['id' => 'submit-answer', 'class' => 'btn btn-lg btn-primary pull-right'])}}
{{ Form::close() }}
@else
<p><h3>Prijavi se kako bi mogao odgovoriti</h3></p>
<a href="/questions/create" class="btn btn-success my-btn-success btn-lg">Login</a>
@endif