<li class="comment">
	<a class="pull-left" href="#">
		<img class="avatar" src="http://bootdey.com/img/Content/user_1.jpg" alt="avatar">
	</a>
	<div class="comment-body">
		<div class="comment-heading">
			<h4 class="user">{{{ $comment->owner->username }}}</h4>
			<h5 class="time">{{ $comment->created_at->diffForHumans() }}</h5>
			<div class="pull-right">

				@if(Auth::check() and (Auth::user()->id == $comment->owner->id))
				<a class="stat-item" href="/comments/{{$comment->id}}/edit" title="Uredi"><span><i class="fa fa-pencil-square-o"></i></span></a>
				@endif

			</div>
		</div>
		<p>{{{ $comment->comment }}}</p>
	</div>

</li>