<aside>
<div class="well">
<h3><span class="label label-default"><i class="fa fa-comments-o"></i>
{{ $stats['questionTotal'] }} pitanja</span></h3>
<h3><span class="label label-default"><i class="fa fa-reply"></i>
{{ $stats['answerTotal'] }} odgovora</span></h3>
<h3><span class="label label-default"><i class="fa fa-users"></i>
{{ $stats['userTotal'] }} korisnika</span></h3>
</div>

    <div class="list-group">

        <a href="#" class="list-group-item my-active">
     <h4>
    </span><i class="fa fa-users"></i>
    Top korisnici
    </h4>
       </a>

        @foreach($topUsers as $topUser)
        <a href="#" class="list-group-item">

            <i class="fa fa-trophy"></i></span> {{ Str::limit($topUser->username, 15)}} <span class="badge my-badge">{{$topUser->answer_count}}</span>

        </a>
        @endforeach()

    </div>



    <div class="list-group">

        <a href="#" class="list-group-item my-active">
<h4><i class="fa fa-star-o"></i>Top tagovi</h4>
       </a>
    <div href="" class="list-group-item">

    @foreach($tags as $tag)
        @include('layout.fragments.tag')
    @endforeach

    </div>

</div>













</div>
</aside>