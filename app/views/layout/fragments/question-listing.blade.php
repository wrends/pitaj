<div class="row question-listing">
            <br>
            <div class="col-md-2 col-sm-3 text-center">
              <a class="story-img" href="#"><img src="/images/ark-help.png" style="width:100px;height:100px;opacity: 0.2;" class="img-circle"></a>
            </div>
            <div class="col-md-10 col-sm-9">
              <h4>{{ HTML::linkRoute('questions.show', $question->title, [$question->id]) }}</h4>
              <div class="row">
                <div class="col-xs-9">

                  @if($question->tags)
                    <p class="pull-right">
                    @foreach($question->tags as $tag)
                      @include('layout.fragments.tag')
                    @endforeach
                    </p>
                  @endif

                  <ul class="list-inline badge"><li><a href="#">{{ $question->created_at->diffForHumans() }}</a></li><li><a href="#"><i class="glyphicon glyphicon-comment"></i> {{ $question->answers->count() }} odgovora</a></li><li><a href="#"><i class="glyphicon glyphicon-share"></i> </a></li></ul>
                  </div>
                <div class="col-xs-3"></div>
              </div>
              <br><br>
            </div>
</div><!-- row -->
<div class="hr-line-dashed"></div>