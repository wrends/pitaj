<div class="row">
	<div class="col-sm-2">
{{ setlocale(LC_TIME, 'Croatian') }}

<div class="voteBox pull-right" style="width:50px;font-size:21pt;text-align:center;">
	<div>
		<a class="vote-up" data-bind="answer-vote-count-{{$answer->id}}" href="/vote/answers/{{$answer->id}}/up" title="">
		<span class="fa fa-chevron-up fa-2x" style="width:50px;font-size:21pt;text-align:center;" title="+1 (Ovo je korisno)"></span>
		</a>
	</div>
	<div style="color:#737373;" id="answer-vote-count-{{$answer->id}}" data-id="answer" title="Number of user votes (likes)">
	{{$answer->vote_count}}
	</div>
	<div>
		<a class="vote-down" data-bind="answer-vote-count-{{$answer->id}}" href="/vote/answers/{{$answer->id}}/down" title="">
		<span class="fa fa-chevron-down fa-2x" style="width:50px;font-size:21pt;text-align:center;" title="-1 (Smanji za 1 glas)"></span>
		</a>
	</div>
</div>



	</div>
	<div class="col-sm-8">
		<div class="panel panel-white post panel-shadow answer">
			<div class="post-heading">
				<div class="pull-left image">
					<img src="http://bootdey.com/img/Content/user_1.jpg" class="img-circle avatar" alt="user profile image">
				</div>

				<div class="pull-left meta">
					<div class="title h5">
						<a href="#"><b>{{{ $answer->owner->username }}}</b></a>
						je dao odgovor
					</div>
					<h6 class="text-muted time">{{ $answer->created_at->diffForHumans() }}</h6>
				</div>
			</div>
			<div class="post-description">
				<p> {{ BBCode::parse($answer->answer) }}</p>
				<div class="stats">
					<a href="/flag/answers/{{$answer->id}}" class="btn btn-default stat-item pull-right">
						<i class="fa fa-flag-o"></i>

					</a>

					<span class="label label-default">

						<i class="fa fa-comment-o"></i> | {{$answer->comments->count()}}

					</span>
					<a href="" class="btn btn-default stat-item toggle-comments">
						<i class="fa fa-share icon"></i>Vidi komentare
					</a>
					@if( ($answer->created_at->timestamp - $answer->updated_at->timestamp) != 0)
						<span class="label label-default">Izmjena: {{$answer->updated_at->formatLocalized('%d %B %Y')}}</span>
					@endif
				@if(Auth::check() and (Auth::user()->id == $answer->owner->id))
				<a class="btn btn-default stat-item" href="/answers/{{$answer->id}}/edit" title="Uredi"><span><i class="fa fa-pencil-square-o"></i></span>Uredi</a>
				@endif

					<span id="loaderImage-{{$answer->id}}"></span>
				</div>
			</div>
			<div class="post-footer">
				<div class="input-group">
					<input id="comment-answer-{{$answer->id}}" data-id="{{$answer->id}}" class="form-control" placeholder="Komentiraj" type="text">
					<span class="input-group-addon">
						<a class="comment-button" data-bind="comment-answer-{{$answer->id}}" href="#"><i class="fa fa-reply"></i></a>
					</span>
				</div>
				@if($answer->comments)
				<ul id="comment-list-{{$answer->id}}" class="comments-list">
				@foreach($answer->comments as $comment)
					@include('layout.fragments.comment')
				@endforeach
				</ul>
				@endif
			</div>
		</div>
</div>
</div>