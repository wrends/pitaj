@if(Auth::check())
<div class="col-sm-8 col-sm-offset-2" style=" padding-bottom: 30px;">

{{ Form::model($answer, ['route' => ['answers.update', $answer->id]] ) }}



<!-- Heading -->
<div class="sub-heading my-heading bg-info text-info">
<h2>Uredi odgovor</h2>
</div>
<div class="hr-line-dashed"></div>
{{ $errors->first() }}
{{ Form::textarea('answer', null, ['id' => 'editor']) }}
{{Form::submit('OK', ['id' => 'submit-answer', 'class' => 'btn btn-lg btn-primary pull-right'])}}
{{ Form::close() }}
@else

@endif