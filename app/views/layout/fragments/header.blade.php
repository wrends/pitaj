<header class="header">
	<div class="container">
		<nav class="navbar navbar-inverse" role="navigation">
			<div class="navbar-header">
				<button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="/" class="navbar-brand scroll-top logo "><img src="/images/question_logo.png" width="55px" height="55px" alt="Pitaj!" style="margin-top:-16px;" ><b>Pitaj!</b></a>
			</div>
			<!--/.navbar-header-->
			<div id="main-nav" class="collapse navbar-collapse">
				<ul class="nav navbar-nav" id="mainNav">
					<li class="{{set_active('/')}}"><a href="/" class=""><i class="fa fa-home color"></i>&nbsp;Naslovnica</a></li>
					<li class="{{set_active('tags')}}"><a href="/tags" class="">Tagovi</a></li>


					@if(Auth::check())
						<li class="dropdown {{ set_active('profile') }}">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-chevron-down"></i>
							<i class="fa fa-user"></i>
							<span>{{ Auth::user()->username }}</span>

						</a>
						<ul class="dropdown-menu">
							<li><a href="/profile">Moj profil</a></li>
							<li class="divider"></li>
							<li><a href="/logout">Logout</a></li>
						 </ul>
					  </li>
				  @else
				 	<li><a href="/register" class="">Registracija</a></li>
					<li><a href="/register" class="scroll-link"><i class="fa fa-sign-in"></i>&nbsp;Login</a></li>
				  @endif

				</ul>
			</div>
			<!--/.navbar-collapse-->
		</nav>
		<!--/.navbar-->
	</div>
	<!--/.container-->
</header>
<!--/.header-->