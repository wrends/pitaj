@extends('layout.default')
@section('content')


<div class="row">
	<div class="container">
		<div class="col-lg-8 col-lg-offset-2">
		<h2>Ukupno {{$tags->getTotal()}} tagova</h2>
					<div class="hr-line-dashed"></div>
			@foreach(array_chunk($tags->all(), 3) as $tagRow)
				<div class="row">
				<h4>
					@foreach($tagRow as $tag)
					<div class="col-md-4">
					<span class="label label-default" style="margin: 10px; background: #555">

						<i class="fa fa-tag"></i>
						<a href="/tag/{{{ $tag->slug }}}" title="{{{ $tag->name }}}">{{{ $tag->name }}}					</a>
						<i class="fa fa-times"></i>
						{{{ $tag->questions->count() }}}

					</span>
					</div>
					@endforeach
				</h4>
				</div><!-- .row -->
			@endforeach
			<div class="hr-line-dashed"></div>
<div class="text-center">{{ $tags->appends(Request::only('q'))->links() }}</div>
		</div>
	</div>
</div>
@stop