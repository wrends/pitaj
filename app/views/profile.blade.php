@extends('layout.default')
@section('content')


<div class="row">
	<div class="container">
		<div class="col-lg-8 col-lg-offset-2">
		<h2><i class="fa fa-user"></i>
{{Auth::user()->username}}</h2>
	<div class="hr-line-dashed"></div>

<div class="tabbable" style="min-height: 400px">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#pane1" data-toggle="tab"><span class=""><i class="fa fa-question-circle"></i>
</span>&nbsp;Moja pitanja</a></li>
    <li><a href="#pane2" data-toggle="tab"><span><i class="fa fa-reply"></i></span>&nbsp;Moji odgovori</a></li>
    <li><a href="#pane3" data-toggle="tab"><span><i class="fa fa-comments-o"></i>
</span>&nbsp;Moji komentari</a></li>
  </ul>
  <div class="tab-content">
    <div id="pane1" class="tab-pane active" style="padding-top: 30px;">

	    @if($questions->count())

	    <table class="table table-hover">
			@foreach($questions as $question)
	    	<tr>
	    		<td>
	    		<a href="/questions/{{$question->id}}" title="{{{ $question->title }}}">{{{ $question->title }}}</a>
	    		</td>
	    		<td>{{{$question->created_at->diffForHumans()}}}</td>
	    		<td>
	    			<a href="/questions/{{$question->id}}/edit" title="Edit"><span><i class="fa fa-pencil-square-o"></i></span></a>
	    		</td>
	    	</tr>
	    	@endforeach
	    	</table>
	    @else
	    	<p>Trenutno nemate pitanja</p>
	    @endif

    </div>
    <div id="pane2" class="tab-pane" style="padding-top: 30px;">
		@if($answers->count())

	    <table class="table table-hover">
			@foreach($answers as $answer)
	    	<tr>
	    		<td>
	    		<a href="/questions/{{$answer->question->id}}" title="">{{{$answer->created_at->diffForHumans()}}}</a></td>
	    		<td>
	    			<a href="/answers/{{$answer->id}}/edit" title="Edit"><span><i class="fa fa-pencil-square-o"></i></span></a>
	    		</td>
	    	</tr>
	    	@endforeach
	    	</table>
	    @else
	    	<p>Trenutno nemate pitanja</p>
	    @endif

    </div>
    <div id="pane3" class="tab-pane" style="padding-top: 30px;">


	    @if($comments->count())

	    <table class="table table-hover">
			@foreach($comments as $comment)
	    	<tr>
	    		<td>
	    		<a href="/questions/{{$comment->question()->id}}" title="">{{{ Str::limit($comment->comment, 25)}}}</a>
	    		</td>
	    		<td>{{{$comment->created_at->diffForHumans()}}}</td>
	    		<td>
	    			<a href="/questions/{{$comment->question()->id}}/edit" title="Edit"><span><i class="fa fa-pencil-square-o"></i></span></a>
	    		</td>
	    	</tr>
	    	@endforeach
	    	</table>
	    @else
	    	<p>Trenutno nemate komantara</p>
	    @endif

    </div>

  </div><!-- /.tab-content -->
</div><!-- /.tabbable -->

		</div>
	</div>
</div>


@stop