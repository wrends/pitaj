@extends('layout.default')
@section('content')
@include('layout.fragments.jumbo')
<div class="row">
            <br>
            <div class="col-md-2 col-sm-3 text-center">
              <a class="story-img" href="#"><img src="//placehold.it/100" style="width:100px;height:100px" class="img-circle"></a>
            </div>
            <div class="col-md-10 col-sm-9">
              <h4>{{{ $question->title }}}</h4>
              <div class="row">
                <div class="col-xs-9">
                <p>{{{ BBCode::parse($question->description) }}}</p>
                  @if($question->tags)
                    <p class="pull-right">
                    @foreach($question->tags as $tag)
                      @include('layout.fragments.tag')
                    @endforeach
                    </p>
                  @endif

                  <ul class="list-inline badge"><li><a href="#">{{ $question->created_at->diffForHumans() }}</a></li><li><a href="#"><i class="glyphicon glyphicon-comment"></i> 4 Comments</a></li><li><a href="#"><i class="glyphicon glyphicon-share"></i> 34 Shares</a></li></ul>
                  </div>
                <div class="col-xs-3"></div>
              </div>
              <br><br>
            </div>
</div><!-- row -->
<div class="hr-line-dashed"></div>
@stop