@extends('layout.default')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="ibox float-e-margins">
                <div class="ibox-content" style="padding-top: 70px;">
                    <div class="text-center">{{ $questions->appends(Request::only('q'))->links() }}</div>
                    <h4>
                        {{ $questions->getTotal() }} rezultata vezano za tag: <span class="text-navy">"{{{ $tag->name }}}"</span>
                    </h4>
                   <!-- <small>Request time  (0.23 seconds)</small> -->
<div class="row">
<div class="col-lg-12">
@include('layout.fragments.search-form')
</div>
</div>

        @if($questions->count())
        	@foreach($questions as $question)
     	 	  	@include('layout.fragments.search-result')
        	@endforeach
       	@endif

                    <div class="hr-line-dashed"></div>

                    <div class="text-center">{{ $questions->appends(Request::only('q'))->links() }}</div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop