<?php namespace QA\Handlers;

use Question;
use Cookie;

class ViewCountHandler {

	public function increment(Question $q)
	{
		if ( Cookie::get('q'.$q->id) !== $q->id ) {
			//dd('no cookie');
			// Update
			$q->increment('views');
			// Set cookie lasting for 30 days
			Cookie::queue('q'.$q->id, $q->id, 60 * 24 * 30);
		}
	}
}