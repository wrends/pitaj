<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AnswersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$usersPool = User::lists('id');
		$questionsPool = Question::lists('id');

		foreach(range(1, 720) as $index)
		{

			$answer = new Answer;
			$answer->user_id = User::find(array_rand($usersPool) + 1)->id;
			$answer->question_id = Question::find(array_rand($questionsPool) + 1)->id;
			$answer->answer = $faker->text;
			$answer->save();
		}
	}

}