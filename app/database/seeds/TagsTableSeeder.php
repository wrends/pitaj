<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class TagsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 70) as $index)
		{
			$slug = implode('-', array_splice((explode('-', $faker->slug)), -2));
			Tag::create([
				'name'		=> $slug,
				'slug'		=> $slug
			]);
		}
	}

}