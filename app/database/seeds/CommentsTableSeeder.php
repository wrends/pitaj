<?php

use Faker\Factory as Faker;

class CommentsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$usersPool = User::lists('id');
		$answersPool = Answer::lists('id');

		foreach(range(1, 100) as $index)
		{

			$comment = new Comment;
			$comment->user_id = User::find(array_rand($usersPool) + 1)->id;
			$comment->answer_id = Answer::find(array_rand($answersPool) + 1)->id;
			$comment->comment = $faker->sentence;
			$comment->save();
		}
	}

}